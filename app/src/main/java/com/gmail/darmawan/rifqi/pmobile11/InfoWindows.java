package com.gmail.darmawan.rifqi.pmobile11;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by yolo on 05/06/18.
 */

public class InfoWindows implements GoogleMap.InfoWindowAdapter {
    private Context context;

    public InfoWindows(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.info_windows, null);

        TextView name_tv = view.findViewById(R.id.nama);
        TextView details_tv = view.findViewById(R.id.detail);
        ImageView img = view.findViewById(R.id.image);

        name_tv.setText(marker.getTitle());
        details_tv.setText(marker.getSnippet());

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        img.setImageResource(imageId);

        return view;
    }
}
