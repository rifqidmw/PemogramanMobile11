package com.gmail.darmawan.rifqi.pmobile11;

/**
 * Created by yolo on 05/06/18.
 */

public class InfoWindowData {
    String nama, detail, image;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
